from trello import TrelloClient
from dateutil import parser
from datetime import timezone
from datetime import datetime
from datetime import timedelta
import argparse
import xlsxwriter

# define args
ap = argparse.ArgumentParser()
ap.add_argument("-s", "--sprint", required=True, help="sprint label")
ap.add_argument("-b", "--board", required=False, help="board id", default="5f7f5e080054370a093fd52d")
ap.add_argument("-a", "--api", required=False, help="api credentials file; ./client_data by default", default="./client_data")
ap.add_argument("-f", "--from", required=False, help="beginning of statistic data if no action is performed before; format YYYY-MM-DD", default=None)
ap.add_argument("-o", "--out", required=False, help="output file", default=None)
ap.add_argument("-p", "--print", required=False, help="print data to the console; false/0 to turn off", default=True)
args = vars(ap.parse_args())

# read args
board_id = args['board']
sprint_name = args['sprint']
client_data_file = args['api']
stats_from = args['from']
out_filename = args['out']
print_data = args['print']

# parse from date
if stats_from is not None:
    try:
        stats_from_date = datetime.strptime(stats_from, "%Y-%m-%d").date()
    except Exception as e:
        print("Cannot parse provided from date. Please use following format YYY-MM-DD. Error message:")
        print(e)
        exit(2)

# get api credentials
with open(client_data_file, "r")as f:
    client_data = eval(f.read())

client = TrelloClient(
    api_key=client_data['api_key'],
    token=client_data['token']
)

sprint_filter = 'label:' + sprint_name
sprint_cards = []

# fetch data
cards = client.search(board_ids=[board_id], models=['cards'], cards_limit=200, query=sprint_filter)
for card in cards:
    card_info = {'id': card.id, 'name': card.name, 'points': 0, 'start': None, 'finish': None}
    sprint_cards.append(card_info)

    # set points
    points = 0
    if len(card.plugin_data) > 0:
        points_plugin = card.plugin_data[0]
        if points_plugin is not None:
            points = eval(points_plugin['value'])['points']
    card_info['points'] = points

    # fetch actions
    card_actions = card.fetch_actions(action_filter='updateCard')

    for action in card_actions:
        # look for moved cards
        if 'listBefore' in action['data']:
            listBefore = action['data']['listBefore']['name']
            listAfter = action['data']['listAfter']['name']

            # get start date
            if listBefore == "To Do":
                start_date = parser.isoparse(action['date'])
                if card_info['start'] is not None:
                    if card_info['start'] > start_date:
                        continue
                card_info['start'] = start_date

            # get finish date
            if listAfter == "Done":
                finish_date = parser.isoparse(action['date'])
                if card_info['finish'] is not None:
                    if card_info['finish'] > finish_date:
                        continue
                card_info['finish'] = finish_date

    # leave date without time data
    card_info['start'] = card_info['start'].replace(tzinfo=timezone.utc).astimezone(tz=None).date() if card_info['start'] is not None else None
    card_info['finish'] = card_info['finish'].replace(tzinfo=timezone.utc).astimezone(tz=None).date() if card_info['finish'] is not None else None

    if print_data is True:
        print("[{points:2}][{start:10}][{finish:10}][{id}] {name}".format(points=card_info['points'],
                                                                          id=card_info['id'],
                                                                          name=card_info['name'],
                                                                          start=card_info['start'].isoformat() if card_info['start'] is not None else "",
                                                                          finish=card_info['finish'].isoformat() if card_info['finish'] is not None else ""))

# print summary to the console
if print_data is True:
    print("\n\tSummary of the {} sprint".format(sprint_name))
    print("Number of tasks in the sprint: {}".format(len(sprint_cards)))
    print("Number of points in the sprint: {}".format(sum(card['points'] for card in sprint_cards)))
    print("Number of task that have not been started yet: {}".format(sum(1 for card in sprint_cards if not card['start'] is not None)))
    print("Number of task that have not been finished yet: {}".format(sum(1 for card in sprint_cards if not card['finish'] is not None)))

# export analysis to the xlsx file
if out_filename is not None:
    # check the filename
    if not out_filename.endswith(".xlsx"):
        out_filename += ".xlsx"

    # setup xlsx file
    workbook = xlsxwriter.Workbook(out_filename)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({'bold': 1})

    # First section - tasks overview
    column = 0
    for header in ['id', 'title', 'points', 'start date', 'finish date']:
        worksheet.write(0, column, header, bold)
        column += 1

    row = 0
    for card in sprint_cards:
        row += 1
        column = 0
        for header in ['id', 'name', 'points']:
            worksheet.write(row, column, card[header])
            column += 1
        worksheet.write(row, column, card['start'].isoformat() if card['start'] is not None else "")
        worksheet.write(row, column+1, card['finish'].isoformat() if card['finish'] is not None else "")

    # prepare min date for second section
    if stats_from is not None:
        min_date = stats_from_date
    else:
        try:
            min_date = min(card['start'] for card in sprint_cards if card['start'] is not None)
        except Exception as e:
            print("No work in progress detected - closing with message:")
            print(e)
            exit(1)

    # Second section - data for burn down chart
    column = 7
    worksheet.write(0, column, 'date', bold)
    worksheet.write(0, column+1, 'in progress', bold)
    worksheet.write(0, column+2, 'done', bold)

    row = 0
    current_date = min_date
    today = datetime.now().date()
    while current_date <= today:
        row +=1
        worksheet.write(row, column, current_date.isoformat())
        in_progress_points = sum(card['points'] for card in sprint_cards if card['start']is None or card['start'] > current_date)
        worksheet.write(row, column + 1, in_progress_points)
        done_points = sum(card['points'] for card in sprint_cards if card['finish']is None or card['finish'] > current_date)
        worksheet.write(row, column + 2, done_points)
        current_date += timedelta(days=1)

    workbook.close()

# available_labels = sorted(label.name for label in client.get_board(board_id).get_labels())
