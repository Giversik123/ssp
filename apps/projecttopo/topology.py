from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSKernelSwitch, UserSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel
from mininet.link import Link, TCLink
import sys


def topology():
    net = Mininet( controller=RemoteController, link=TCLink, switch=OVSKernelSwitch )

    # Add hosts, servers as hosts, switches and controller
    h1 = net.addHost('h1', ip="192.168.1.1/24", mac="00:00:00:00:00:01")
    h2 = net.addHost('h2', ip="192.168.1.2/24", mac="00:00:00:00:00:02")

    server1 = net.addHost('server1', ip="192.168.1.11/24", mac="00:00:00:00:00:11")
    server2 = net.addHost('server2', ip="192.168.1.12/24", mac="00:00:00:00:00:12")
    server3 = net.addHost('server3', ip="192.168.1.13/24", mac="00:00:00:00:00:13")
    server4 = net.addHost('server4', ip="192.168.1.14/24", mac="00:00:00:00:00:14")
    server5 = net.addHost('server5', ip="192.168.1.15/24", mac="00:00:00:00:00:15")
    server6 = net.addHost('server6', ip="192.168.1.16/24", mac="00:00:00:00:00:16")

    s1 = net.addSwitch('s1')
    s2 = net.addSwitch('s2')
    s3 = net.addSwitch('s3')
    s4 = net.addSwitch('s4')
    s5 = net.addSwitch('s5')

    controller_ip = sys.argv[1]
    c0 = net.addController('c0', controller=RemoteController, ip=controller_ip, port=6653)

    # Add links between devices
    net.addLink(h1, s1)
    net.addLink(h2, s1)

    net.addLink(s1, s2)
    net.addLink(s1, s3)

    net.addLink(s2, s4)
    net.addLink(s2, s5)
    net.addLink(s3, s4)
    net.addLink(s3, s5)

    net.addLink(s4, server1)
    net.addLink(s4, server2)
    net.addLink(s4, server3)

    net.addLink(s5, server4)
    net.addLink(s5, server5)
    net.addLink(s5, server6)

    # Start devices
    net.build()
    c0.start()
    s1.start([c0])
    s2.start([c0])
    s3.start([c0])
    s4.start([c0])
    s5.start([c0])

    print('*** Running CLI')
    CLI(net)
    print('*** Stopping network')
    net.stop()


if __name__ == '__main__':
    setLogLevel('info')
    topology()